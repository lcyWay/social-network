import React from 'react';
import styled from 'styled-components'

type typeButton = {
  firstButtonContent: any;
  secondButtonContent: any;
  onClick?: () => void;
  disabled?: boolean;
  active: boolean;
};

export const TypeGroupButton = ({
  disabled,
  onClick,
  active,
  firstButtonContent,
  secondButtonContent,
}: typeButton) => {
  return (
    <BorderContainer>
      <Button
        position="left"
        active={active}
        onClick={() => onClick ? onClick() : null}
      >
        {firstButtonContent}
      </Button>
      <Button
        active={true}
        position="right"
      >
        {secondButtonContent}
      </Button>
    </BorderContainer>
  )
}

const BorderContainer = styled.div`
  display: flex;
  padding: 2px;
  border-radius: 6px;
  background: ${({ theme }) => theme.colors.primary};
`;

const Button = styled.button<{ position: "left" | "right", active: boolean }>`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 4px 14px;
  border: 0;
  cursor: pointer;
  font-size: 12px;
  border-radius: ${({ position }) => position === "left" ? "4px 0 0 4px" : "0 4px 4px 0"};
  background: ${({ theme, active }) => active ? theme.colors.primary : theme.layouts.primary};
  color: ${({ theme }) => theme.typography.white};
  letter-spacing: 1px;

  &:hover {
    background: ${({ theme, position, active }) => position === "right"
    ? theme.colors.primary
    : active
      ? theme.layouts.primary
      : theme.colors.primary
  };
  }
  
  @media (min-width: 768px) {
    font-size: 14px;
  };

  @media (min-width: 1200px) {
    font-size: 18px;
  };
`;

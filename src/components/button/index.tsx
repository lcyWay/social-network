import React from 'react';
import styled from 'styled-components'

type typeButton = {
  children: any;
  disabled?: boolean;
  margin?: string;
  onClick?: () => void;
};

export const TypeButton = ({
  children,
  disabled = false,
  margin,
  onClick
}: typeButton) => {
  return (
    <BorderContainer margin={margin} disabled={disabled}>
      <Button
        disabled={disabled}
        onClick={(e) => {
          e.stopPropagation();
          onClick && onClick();
        }}
      >
        {children}
      </Button>
    </BorderContainer>
  )
}

const BorderContainer = styled.div<{ disabled: boolean, margin: string | undefined }>`
  cursor: ${({ disabled }) => disabled ? "default" : "pointer"};
  display: flex;
  padding: 2px;
  margin: ${({ margin }) => margin || "0"};
  border-radius: 6px;
  background: ${({ theme, disabled }) => disabled ? theme.layouts.hover : theme.colors.primary};
`;

const Button = styled.button<{ disabled: boolean }>`
  cursor: ${({ disabled }) => disabled ? "default" : "pointer"};
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 4px 14px;
  border: 0;
  font-size: 12px;
  border-radius: 4px;
  background: ${({ theme }) => theme.layouts.primary};
  color: ${({ theme, disabled }) => disabled ? theme.typography.hint : theme.typography.white};
  letter-spacing: 1px;

  &:hover {
    background: ${({ theme, disabled }) => disabled ? theme.layouts.primary : theme.colors.primary};
  }

  @media (min-width: 768px) {
    font-size: 14px;
  };

  @media (min-width: 1200px) {
    font-size: 18px;
  };
`;

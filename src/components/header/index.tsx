import { User } from '@/types/user';
import Link from 'next/link';
import router from 'next/router';
import React from 'react'
import styled from 'styled-components'
import { TypeButton } from '../button';

type iProps = {
  user: User;
};

const Header: React.FC<iProps> = ({ user }) => {
  const handleLogout = React.useCallback(() => {
    document.cookie = "authCpwToken= ;path=/ ;expires=Thu, 01 Jan 1970 00:00:01 GMT";
    router.replace('/auth/login');
  }, []);

  return (
    <HeaderContainer>
      <LogoContainer>
        Приветствуем,<UserNameContainer>
          <Link href={`/profile/${user._id}`}>
            <a>
              {user.name}
            </a>
          </Link>
        </UserNameContainer>
      </LogoContainer>
      <ButtonsContainer>
        {
          user !== null ? (
            <TypeButton onClick={handleLogout}>Выйти</TypeButton>
          ) : (
            <TypeButton>Войти</TypeButton>
          )
        }
      </ButtonsContainer>
    </HeaderContainer>
  )
}

const HeaderContainer = styled.div`
  background: ${({ theme }) => theme.layouts.primary};
  height: 50px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 0 10px;
  color: ${({ theme }) => theme.typography.white};
`;

const LogoContainer = styled.div`
  font-size: 12px;
  letter-spacing: 1px;
  cursor: default;
  display: flex;
  align-items: center;

  @media (min-width: 768px) {
    font-size: 16px;
  };

  @media (min-width: 1200px) {
    font-size: 20px;
  };
`;
const UserNameContainer = styled.div`
  cursor: pointer;
  margin-left: 4px;
  border-bottom: 4px dotted transparent;
  position: relative;
  top: 2px;

  &:hover {
    top: 0px;
    border-bottom: 4px dotted white;
  }
`;
const ButtonsContainer = styled.div`

`;

export default Header

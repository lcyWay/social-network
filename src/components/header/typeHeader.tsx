import React from "react";
import styled from "styled-components";

type iProps = {
  children: any;
};

export const TypeHeader = ({
  children,
}: iProps) => {
  return (
    <Header>
      <InputContainer>
        {children}
      </InputContainer>
    </Header>
  )
}

const Header = styled.div`
  width: calc(100% - 10px);
  background: ${({ theme }) => theme.layouts.secondary};
  color: ${({ theme }) => theme.typography.white};
  padding: 5px;
`;
const InputContainer = styled.div`
  display: flex;
  margin: auto;
  align-items: center;
  justify-content: center;
  max-width: 400px;
`;
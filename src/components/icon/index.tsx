import React from 'react'
// import Image from 'next/image'
import styled from 'styled-components';
import ClipLoader from "react-spinners/ClipLoader";
import NullUserIcon from '../../../public/profile-user.svg'

type iProps = {
  src: string;
  isLoaded: boolean;
  width?: number;
  height?: number;
  isRounded?: boolean;
  staticBorderColor?: string;
  hoverBorderColor?: string;
  onClick?: () => void;
};

export const TypeIcon = ({
  src,
  isLoaded,
  staticBorderColor,
  hoverBorderColor,
  width = 30,
  height = 30,
  isRounded = true,
  onClick,
}: iProps) => {
  const iconSrc = src === null ? NullUserIcon : src;

  return (
    <Container>
      {
        isLoaded || src === null
          ? (
            <ImageContainer
              onClick={(e) => {
                e.stopPropagation();
                onClick && onClick();
              }}
              width={width}
              isRounded={isRounded}
              staticBorderColor={staticBorderColor}
              hoverBorderColor={hoverBorderColor}
            >
              <Image src={iconSrc} width={width} height={height} />
            </ImageContainer>
          )
          : <ClipLoader color={"white"} loading={true} size={width > 40 ? 40 : width} />
      }
    </Container>
  )
}

const Container = styled.div`
  display: flex;
`;
const ImageContainer = styled.div<{ isRounded: boolean, staticBorderColor: string, hoverBorderColor: string, width: number }>`
  display: flex;
  align-items: center;
  width: ${({ width }) => width * 0.75 + "px"};
  border: 2px solid ${({ theme, staticBorderColor }) => staticBorderColor ? theme.layouts[staticBorderColor] : theme.layouts.hover};
  border-radius: ${({ isRounded }) => isRounded ? "50%" : "0"};

  &:hover {
    border: 2px solid ${({ theme, hoverBorderColor }) => hoverBorderColor ? theme.layouts[hoverBorderColor] : theme.colors.primary};
  }

  img, svg {
    border-radius: ${({ isRounded }) => isRounded ? "50%" : "0"};
    object-fit: cover;
  }

  @media (min-width: 768px) {
    width: ${({ width }) => width * 0.85 + "px"};
  };

  @media (min-width: 1200px) {
    width: ${({ width }) => width + "px"};
  };
`;

const Image = styled.img<{ width: number, height: number }>`
  width: ${({ width }) => width * 0.75 + "px"};
  height: ${({ height }) => height * 0.75 + "px"};

  @media (min-width: 768px) {
    width: ${({ width }) => width * 0.85 + "px"};
    height: ${({ height }) => height * 0.85 + "px"};
  };

  @media (min-width: 1200px) {
    width: ${({ width }) => width + "px"};
    height: ${({ height }) => height + "px"};
  };
`;

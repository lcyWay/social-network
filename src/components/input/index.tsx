import styled from "styled-components";

type iProps = {
  value: string;
  onChange?: (value: string) => void;
  staticColor?: "primary" | "secondary" | "hover";
  focusedColor?: "primary" | "secondary" | "hover";
  ph?: string;
  id?: string;
};

export const TypeInput = ({
  value,
  staticColor = "primary",
  focusedColor = "secondary",
  onChange,
  id,
  ph,
}: iProps) => {
  return (
    <Input
      id={id || ""}
      staticColor={staticColor}
      focusedColor={focusedColor}
      placeholder={ph || ""}
      value={value}
      onChange={(v) => onChange ? onChange(v.target.value) : null}
    />
  )
}

const Input = styled.input<{ staticColor: null | string, focusedColor: null | string }>`
  border: 0;
  padding: 8px 10px;
  border-radius: 6px;
  letter-spacing: 1px;
  font-size: 12px;
  width: calc(100% - 20px);
  color: ${({ theme }) => theme.typography.white};
  background: ${({ theme, staticColor }) => theme.layouts[staticColor]};

  &:focus {
    background: ${({ theme, focusedColor }) => theme.layouts[focusedColor]};
  };

  @media (min-width: 768px) {
    font-size: 14px;
  };

  @media (min-width: 1200px) {
    font-size: 18px;
  };
`;

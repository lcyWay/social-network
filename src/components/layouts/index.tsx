import styled from 'styled-components';

const Container = styled.div`
  width: 100%;
  min-height: calc(100vh - 50px);
  background: ${({ theme }) => theme.layouts.primary};
`;

const ContentContainer = styled.div<{ size?: "small" | "default" }>`
  margin: auto;
  max-width: ${({ size }) => size === "small" ? "650px" : "800px"};
  width: calc(100% - 20px);
  padding: 10px;
`;

export const MainLayout = {
  Container,
  ContentContainer
};

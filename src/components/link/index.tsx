import Link from "next/link";

type iProps = {
  children: any;
  href: string;
};

export const TypeLink = ({
  children,
  href
}: iProps) => (
  <Link href={href}>
    <a>
      {children}
    </a>
  </Link>
)

import React from 'react'
import styled from 'styled-components'
import { TypeButton } from '../button';

interface iProps {
  isOpen: boolean;
  label: string;
  children: any;
  onSubmit?: () => void;
  onClose?: () => void;
};

export const TypeModal: React.FC<iProps> = ({ label, children, onSubmit, onClose, isOpen }) => {
  const preventClose = React.useCallback((e) => {
    e.stopPropagation();
  }, []);

  return isOpen && (
    <BlurContainer onPointerDown={onClose}>
      <ModalContainer onPointerDown={preventClose}>
        <LabelContainer>
          {label}
        </LabelContainer>
        <ContentContainer>
          {children}
        </ContentContainer>
        <ButtonsContainer>
          <TypeButton onClick={onClose}>Отмена</TypeButton>
          <TypeButton onClick={onSubmit}>Сохранить изменения</TypeButton>
        </ButtonsContainer>
      </ModalContainer>
    </BlurContainer>
  )
}

const BlurContainer = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100vw;
  height: 100%;
  z-index: 10;
  background: rgba(0,0,0,0.5);
  cursor: pointer;
`;
const ModalContainer = styled.div`
  cursor: auto;
  max-width: 800px;
  width: 100%;
  max-height: 800px;
  background: ${({ theme }) => theme.layouts.white};
`;
const LabelContainer = styled.div`
  background: ${({ theme }) => theme.layouts.primary};
  color: ${({ theme }) => theme.typography.white};
  height: 32px;
  padding: 4px;
  display: flex;
  align-items: center;
  font-size: 18px;
`;
const ContentContainer = styled.div`
  max-height: calc(100% - 96px);
`;
const ButtonsContainer = styled.div`
  background: ${({ theme }) => theme.layouts.primary};
  display: flex;
  padding: 0 20px;
  height: 56px;
  justify-content: space-between;
  align-items: center;
`;
import React from 'react';
import Link from 'next/link';
import styled from 'styled-components';

const index = () => {
  return (
    <NavbarContainer>
      <Link href="/">
        <a>
          <NavItem>
            Профиль
          </NavItem>
        </a>
      </Link>
      <Link href="/users">
        <a>
          <NavItem>
            Пользователи
          </NavItem>
        </a>
      </Link>
      <Link href="/dialogs">
        <a>
          <NavItem>
            Сообщения
          </NavItem>
        </a>
      </Link>
    </NavbarContainer>
  )
}

const NavbarContainer = styled.div`
  width: calc(150px - 20px);
  min-height: calc(100vh - 100px);
  display: none;
  padding: 25px 10px;
  flex-direction: column;
  background: ${({ theme }) => theme.layouts.secondary};

  a {
    margin-bottom: 10px;
  }

  @media (min-width: 768px) {
    display: flex;
  };

  @media (min-width: 1200px) {
    width: calc(300px - 20px);
  };
`;

const NavItem = styled.div`
  display: flex;
  color: ${({ theme }) => theme.typography.white};
  font-size: 12px;
  letter-spacing: 2px;
  padding: 10px 20px;
  border-radius: 6px;
  &:hover {
    background: ${({ theme }) => theme.layouts.hover};
  }

  @media (min-width: 1200px) {
    font-size: 18px;
  };
`;

export default index

import React from 'react'
import styled from 'styled-components';

const NewsBar = () => {
  return (
    <Wrapper>
      <div>Что-то еще...</div>
    </Wrapper>
  )
}

const Wrapper = styled.div`
  display: none;
  width: 150px;
  background: #2b2b2df7;

  @media (min-width: 768px) {
    display: flex;
  };

  @media (min-width: 1200px) {
    width: 300px;
  };
`;

export default NewsBar

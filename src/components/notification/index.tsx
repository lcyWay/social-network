import React from 'react';
import styled from 'styled-components'

import { TypeIcon } from '../icon';
import { TypeLink } from '../link';

import { Message } from '@/types/dialogs';
import { SocketNotification } from '@/types/socket';
import { useLoadUserData } from 'src/hooks/useLoadUserData';

type StateProps = {
  stacks: any,
  stacksQueue: any[],
  counter: number,
  maxStacks: number,
  context: {
    showNotification: (a: any, b: any, c?: any) => void
  },
};
type ProviderProps = {
  showNotification: (event: SocketNotification) => void
};

const ContextProvider = React.createContext<any>('');

class NotificationProvider extends React.Component<any, StateProps> {
  constructor(props) {
    super(props);
    this.state = {
      stacks: {},
      stacksQueue: [],
      counter: 0,
      maxStacks: 5,
      context: {
        showNotification: this.showNotification
      }
    };
  };

  showNotification = (
    event: SocketNotification
  ) => {
    const { data } = event;
    let id: number;

    this.setState(state => {
      if (Object.keys(state.stacks).length < state.maxStacks) {
        id = state.counter;
        return { ...state, stacks: { ...state.stacks, [state.counter]: data }, counter: state.counter + 1 };
      } else {
        return { ...state, stacksQueue: [...state.stacksQueue, data] };
      };
    });

    setTimeout(() => {
      this.setState(prev => {
        const newStacks = { ...prev.stacks };
        delete newStacks[id];
        return { ...prev, stacks: newStacks }
      });
      if (this.state.stacksQueue.length > 0) {
        this.showNotification(event);
        this.setState(prev => {
          const newStacks = [...prev.stacksQueue];
          newStacks.splice(0, 1);
          return { ...prev, stacksQueue: newStacks }
        })
      };
    }, 3000);
  };

  render() {
    return <Container>
      <ContextProvider.Provider value={this.state.context}>
        {this.props.children}
        <NotificationsContainer>
          <NotificatoinsComponent stacks={this.state.stacks} />
        </NotificationsContainer>
      </ContextProvider.Provider>
    </Container>
  }
}

const NotificatoinsComponent: React.FC<{ stacks: Message[] }> = ({ stacks }) => {
  const [users, usersQueries, fetchUser] = useLoadUserData();
  return <>
    {
      Object.keys(stacks).map((key) => {
        const data: Message = stacks[key];
        const userData = users[data.userId];
        const iconSrc = userData?.iconSrc;

        if (!userData && !usersQueries.has(data.userId)) {
          fetchUser(data.userId);
        }

        return (
          <NotificationCard key={data._id}>
            <EventMessage>{`У вас новое сообщение от ${userData?.name || "..."}:`}</EventMessage>
            <MessageContainer>
              <TypeLink href={`/profile/${data.userId}`}>
                <TypeIcon staticBorderColor={"primary"} src={iconSrc} isLoaded={!!iconSrc} />
              </TypeLink>
              <TextContainer>{data.message}</TextContainer>
            </MessageContainer>
          </NotificationCard>
        )
      })
    }
  </>
}

const useNotification = (): ProviderProps => React.useContext(ContextProvider);
export { NotificationProvider, useNotification };



const Container = styled.div`
  position: relative;
`;
const NotificationsContainer = styled.div`
  position: absolute;
  bottom: 10px;
  left: 10px;
`;


const NotificationCard = styled.div`
  color: white;
  background: ${({ theme }) => theme.colors.secondary};

  width: 300px;
  min-height: 50px;
  border-radius: 6px;
  
  padding: 5px 10px;
  margin-top: 10px;
`;

const EventMessage = styled.div`
  margin-bottom: 2px;
  font-size: 16px;
`;

const MessageContainer = styled.div`
  display: flex;
  align-items: center;
`;

const TextContainer = styled.div`
  margin-left: 8px;
`;

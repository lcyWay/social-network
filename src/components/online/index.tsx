import React from 'react';
import styled from 'styled-components';

type iProps = {
  status: boolean;
};

export const TypeOnlineStatus: React.FC<iProps> = ({ status }) => {
  return status ? (
    <StatusContainer>
      <Status status={true} />
    </StatusContainer>
  ) : (
    <StatusContainer>
      <Status status={false} />
    </StatusContainer>
  );
};

const StatusContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 0 5px;
`;

const Status = styled.div<{ status: boolean }>`
  display: flex;
  width: 10px;
  height: 10px;
  margin-top: 2px;
  border-radius: 50%;
  background: ${({ theme, status }) => status ? theme.colors.green : theme.colors.gray};
`;

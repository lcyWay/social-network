import styled from "styled-components";

type iProps = {
  children: any;
  margin?: string | null;
};

export const TypeLinkText = ({
  children,
  margin = null
}: iProps) => (
  <LinkText margin={margin}>{children}</LinkText>
);

const LinkText = styled.div<{ margin: string | null }>`
  letter-spacing: 1px;
  color: ${({ theme }) => theme.typography.white};
  border-bottom: 4px dotted transparent;
  position: relative;
  top: 2px;
  margin: ${({ margin }) => margin || "0"};

  &:hover {
    top: 0px;
    border-bottom: 4px dotted white;
  }
`;

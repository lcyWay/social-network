import { theme } from "src/theme";
import styled from "styled-components";

type iProps = {
  children: any;
  onClick?: () => any;
  width?: number;
  staticColor?: "secondary" | "primary" | "hover" | null;
  hoverColor?: "secondary" | "primary" | "hover";
};

export const TypeToolButton = ({
  children,
  hoverColor = "secondary",
  staticColor,
  onClick,
  width = 30,
}: iProps) => {
  return (
    <Button
      width={width}
      hoverColor={hoverColor}
      staticColor={staticColor}
      onClick={() => onClick ? onClick() : null}
    >
      {children}
    </Button>
  )
}

const Button = styled.div<{ hoverColor: string, width: number, staticColor: string | null }>`
  cursor: pointer;
  background: ${({ staticColor }) => staticColor ? theme.colors[staticColor] : "inherit"};
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 50%;
  padding: 4px;
  width: ${({ width }) => width + "px"};
  height: ${({ width }) => width + "px"};
  color: ${({ theme }) => theme.typography.white};

  svg, img {
    width: ${({ width }) => width + "px"};
  }

  &:hover {
    background: ${({ theme, hoverColor }) => theme.layouts[hoverColor]};
  }
`;

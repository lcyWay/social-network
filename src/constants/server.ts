export const serverURL = process.env.NEXT_PUBLIC_IS_DEV === "development"
  ? "http://localhost:4000/"
  : "https://social-network-carepuw.herokuapp.com/";
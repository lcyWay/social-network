import React from 'react'
import Image from 'next/image'
import styled from 'styled-components';
import dayjs from 'dayjs';
import { api } from '@/helpers/api';

import { useSocket } from 'src/hooks/useSocket';
import { useLoadUserData } from 'src/hooks/useLoadUserData';

import { User } from '@/types/user';

import { MainLayout } from '@/components/layouts';
import { TypeHeader } from '@/components/header/typeHeader';
import { TypeInput } from '@/components/input';
import { TypeToolButton } from '@/components/toolButton';
import { TypeLink } from '@/components/link';
import { TypeLinkText } from '@/components/text';
import { TypeIcon } from '@/components/icon';

import sendPic from '../../../public/chevron_right_black_24dp.svg';
import deletePic from '../../../public/highlight_off_black_24dp.svg';
import { TypeOnlineStatus } from '@/components/online';

type iProps = {
  user: User;
  dialogId: string;
};

const DialogContainer: React.FC<iProps> = ({ user, dialogId }) => {
  const [messages, setMessages] = React.useState([]);
  const [messageValue, setMessageValue] = React.useState("");
  const [users, usersQueries, fetchUser] = useLoadUserData();
  const [socket] = useSocket(user._id, "dialog", dialogId);
  const currentDialog = user.dialogs.find(e => e._id === dialogId);
  const chatCompanion = users[currentDialog.companionId];

  React.useEffect(() => {
    if (!usersQueries.has(currentDialog.companionId)) {
      fetchUser(currentDialog.companionId);
    };
    api.post('dialog', { dialogId }).then((res) => {
      if (res.success) {
        setMessages(prev => ([...prev, ...res.data.messages]))
      }
    });
    if (currentDialog.unreadMessages) {
      socket.emit('VIEW_MESSAGES');
    }
  }, []);

  React.useEffect(() => {
    const input = document.getElementById('dialog-input');
    input.focus();
  }, []);

  React.useEffect(() => {
    socket.emit('JOIN_ROOM', dialogId);
    socket.on('UPDATE_MESSAGES', (messages) => {
      setMessages(messages);
    });
    return function () {
      socket.off('UPDATE_MESSAGES');
      socket.emit('LEAVE_ROOM', dialogId);
    }
  }, [dialogId, user._id]);

  React.useEffect(() => {
    const target = document.getElementById("scroll-target");
    target.scrollIntoView({ block: "end", inline: "end" });
  }, [users, messages]);

  React.useEffect(() => {
    document.addEventListener('keydown', enterDown);
    return function () {
      document.removeEventListener('keydown', enterDown);
    }
  }, [socket, messageValue]);



  const enterDown = (e) => {
    if (e.code === 'Enter') {
      handleSend();
    }
  };

  const handleSend = React.useCallback(() => {
    if (messageValue.trim() !== "") {
      const data = {
        message: messageValue,
        userId: user._id,
        userName: user.name,
        date: new Date(),
      }
      socket.emit('NEW_MESSAGE', { data, chatCompanionId: currentDialog.companionId });
      setTimeout(() => {
        setMessageValue("");
      });
    }
  }, [messageValue, user, socket]);

  const handleDelete = React.useCallback((id) => {
    socket.emit('DELETE_MESSAGE', { id, chatCompanionId: currentDialog.companionId });
  }, [socket]);

  return (
    <MainLayout.Container>
      <TypeHeader>
        Диалог с
        {
          chatCompanion && <>
            <TypeOnlineStatus status={chatCompanion.online} />
            <TypeLinkText margin="0 5px 0 0">
              <TypeLink href={`/profile/${chatCompanion._id}`}>
                {chatCompanion.name}
              </TypeLink>
            </TypeLinkText>
            <TypeLink href={`/profile/${chatCompanion._id}`}>
              <TypeIcon isLoaded={!!chatCompanion.iconSrc} src={chatCompanion.iconSrc} />
            </TypeLink>
          </>
        }
        <MessagesCounter>Сообщений: {messages.length}</MessagesCounter>
      </TypeHeader>
      <MainLayout.ContentContainer>
        <MessagesFlexContainer>
          <MessagesContainer id="test">
            {
              messages.map((message, i) => {
                const prevMessage = messages[i - 1];
                const isUserMessage = user._id === message.userId;
                const userData = users[message.userId];
                const iconSrc = userData?.iconSrc;

                if (!usersQueries.has(message.userId)) {
                  fetchUser(message.userId);
                };

                return (prevMessage ? prevMessage.userId !== message.userId : true) ? (
                  <UserCard reverse={isUserMessage} key={message._id}>
                    <MessageCardContainer isUserMessage={isUserMessage} size="big">
                      <MessageHeader>
                        <TypeLink href={`/profile/${message.userId}`}>
                          <TypeIcon
                            hoverBorderColor={isUserMessage ? "secondary" : null}
                            src={iconSrc}
                            isLoaded={!!iconSrc}
                            width={30}
                          />
                        </TypeLink>
                        <UserCardTextContainer>
                          <TypeLink href={`/profile/${message.userId}`}>
                            <TypeLinkText>{message.userName}</TypeLinkText>
                          </TypeLink>
                        </UserCardTextContainer>
                        <MessageHeaderDate isUserMessage={isUserMessage}>
                          {dayjs(message.date).format('DD.MM.YY HH:mm')}
                        </MessageHeaderDate>
                      </MessageHeader>
                      <MessageContent>{message.message}</MessageContent>
                    </MessageCardContainer>
                    {isUserMessage && (
                      <DeleteButtonContainer>
                        <TypeToolButton
                          onClick={handleDelete.bind(null, message._id)}
                        >
                          <Image src={deletePic} />
                        </TypeToolButton>
                      </DeleteButtonContainer>
                    )}
                  </UserCard>
                ) : (
                  <SmallMessageContainer reverse={isUserMessage} key={message._id}>
                    <MessageCardContainer isUserMessage={isUserMessage} size="small">
                      {message.message}
                      <MessageHeaderDate isUserMessage={isUserMessage} size="small">
                        {dayjs(message.date).format('HH:mm')}
                      </MessageHeaderDate>
                      {isUserMessage && (
                        <DeleteButtonContainer>
                          <TypeToolButton
                            onClick={handleDelete.bind(null, message._id)}
                          >
                            <Image src={deletePic} />
                          </TypeToolButton>
                        </DeleteButtonContainer>
                      )}
                    </MessageCardContainer>
                  </SmallMessageContainer>
                )
              })
            }
            <div id="scroll-target"></div>
          </MessagesContainer>
        </MessagesFlexContainer>
        <InputContainer>
          <TypeInput
            id="dialog-input"
            onChange={setMessageValue}
            value={messageValue}
            ph="Вы можете что-нибудь написать..."
          />
          <ToolButtonWrapper>
            <TypeToolButton onClick={handleSend}>
              <Image src={sendPic} />
            </TypeToolButton>
          </ToolButtonWrapper>
        </InputContainer>
      </MainLayout.ContentContainer>
    </MainLayout.Container>
  )
}

const UserCard = styled.div<{ reverse?: boolean }>`
  margin-top: 10px;
  position: relative;
  display: flex;
  flex-direction: ${({ reverse }) => reverse ? "row-reverse" : "row"};
  padding-left: ${({ reverse }) => reverse ? "20%" : "0"};
  padding-right: ${({ reverse }) => reverse ? "0" : "20%"};
`;
const MessageCardContainer = styled.div<{ size: "small" | "big", isUserMessage: boolean }>`
  padding: ${({ size }) => size === "small" ? "6px 10px" : "10px"};
  border-radius: 6px;
  display: flex; 
  flex-direction: ${({ size }) => size === "small" ? "row" : "column"};
  background: ${({ theme, isUserMessage }) => !isUserMessage ? theme.layouts.secondary : theme.colors.secondary};
  letter-spacing: 0.25px;
  font-size: 16px;
`;

const MessagesFlexContainer = styled.div`
  height: calc(100vh - 186px);
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  max-height: calc(100vh - 186px);
  margin: auto;
  padding: 0 75px;
  position: relative;
  overflow: scroll;

  &::-webkit-scrollbar {
    width: 0em;
    height: 0em;
  }
   
  &::-webkit-scrollbar-track {
    width: 0em;
    height: 0em;
  }
   
  &::-webkit-scrollbar-thumb {
    width: 0em;
    height: 0em;
    outline: 0;
  }
`;
const MessagesContainer = styled.div`
  max-height: calc(100vh - 186px);
`;
const SmallMessageContainer = styled.div<{ reverse?: boolean }>`
  margin-top: 5px;
  position: relative;
  display: flex;
  flex-direction: ${({ reverse }) => reverse ? "row-reverse" : "row"};
  color: ${({ theme }) => theme.typography.white};
  padding-left: ${({ reverse }) => reverse ? "20%" : "0"};
  padding-right: ${({ reverse }) => reverse ? "0" : "20%"};
`;

const InputContainer = styled.div`
  display: flex;
  margin-top: 10px;
  padding: 0 75px;
`;
const ToolButtonWrapper = styled.div`
  margin-left: 10px;
`;

const UserCardTextContainer = styled.div`
  display: flex;
  flex: 1;
  margin-right: 25px;
  margin-left: 6px;
  font-size: 16px;
`;
const MessageHeader = styled.div`
  display: flex;
  align-items: center;
`;
const MessageContent = styled.div`
  margin-top: 6px;
  color: ${({ theme }) => theme.typography.white};
  letter-spacing: 1px;
  font-size: 16px;
`;
const MessageHeaderDate = styled.div<{ size?: "small", isUserMessage: boolean }>`
  color: ${({ theme, isUserMessage }) => isUserMessage ? theme.typography.lightDark : theme.typography.hint};
  display: flex;
  justify-content: flex-end;
  position: relative;
  top: ${({ size }) => size ? "8px" : "0"};
  margin-left: 5px;
  font-size: ${({ size }) => size ? "12px" : "13px"};
`;
const DeleteButtonContainer = styled.div`
  position: absolute;
  right: -48px;
  top: 0;
`;

const MessagesCounter = styled.div`
  margin-left: 35px;
`;

export default DialogContainer

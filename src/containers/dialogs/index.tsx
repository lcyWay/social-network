import React from 'react'
import styled from 'styled-components';

import { useLoadUserData } from 'src/hooks/useLoadUserData';
import { useSocket } from 'src/hooks/useSocket';

import { TypeHeader } from '@/components/header/typeHeader';
import { MainLayout } from '../../components/layouts'
import { TypeLink } from '@/components/link';
import { TypeIcon } from '@/components/icon';
import { TypeLinkText } from '@/components/text';
import { TypeToolButton } from '@/components/toolButton';
import { TypeInput } from '@/components/input';
import { TypeOnlineStatus } from '@/components/online';

import { Dialog } from '@/types/dialogs';
import { User } from '@/types/user';

type iProps = {
  user: User
};

const DialogsContainer = ({ user }: iProps) => {
  const [users, usersQueries, fetchUser] = useLoadUserData();
  const [filterValue, setFilterValue] = React.useState("");
  useSocket(user._id, "dialogs");

  React.useEffect(() => {
    fetchUser(user._id);
  }, []);

  return (
    <MainLayout.Container>
      <TypeHeader>
        <TypeInput
          staticColor="secondary"
          value={filterValue}
          onChange={setFilterValue}
          ph="Поиск по имени..."
        />
      </TypeHeader>
      <MainLayout.ContentContainer>
        {
          user.dialogs.map((dialog: Dialog) => {
            const userData: User = users[dialog.companionId];
            const iconSrc = userData?.iconSrc;
            const lastMessageUser = users[dialog.lastMessage.userId];
            const lastMessageIconSrc = lastMessageUser?.iconSrc;

            if (!userData && !usersQueries.has(dialog.companionId)) {
              fetchUser(dialog.companionId);
            };

            if (!lastMessageUser && !usersQueries.has(dialog.lastMessage.userId)) {
              fetchUser(dialog.companionId);
            };

            return (
              <TypeLink key={dialog._id} href={`/dialogs/${dialog._id}`}>
                <UserCard>
                  <HeaderContainer>
                    <TypeLink href={`/profile/${dialog.companionId}`}>
                      <TypeIcon
                        src={iconSrc}
                        isLoaded={!!iconSrc}
                        width={30}
                      />
                    </TypeLink>
                    {userData &&
                      <UserCardTextContainer>
                        <TypeLink href={`/profile/${dialog.companionId}`}>
                          <TypeLinkText>{userData.name}</TypeLinkText>
                        </TypeLink>
                        <TypeOnlineStatus status={userData.online} />
                      </UserCardTextContainer>
                    }
                    <TypeToolButton width={22} staticColor={dialog.unreadMessages ? "primary" : null} hoverColor="primary">
                      {dialog.unreadMessages}
                    </TypeToolButton>
                  </HeaderContainer>
                  <LastMessageContainer>
                    <TypeLink href={`/profile/${dialog.lastMessage.userId}`}>
                      <TypeIcon isLoaded={!!lastMessageIconSrc} src={lastMessageIconSrc} />
                    </TypeLink>
                    <LastMessageText>
                      {dialog.lastMessage.message}
                    </LastMessageText>
                  </LastMessageContainer>
                </UserCard>
              </TypeLink>
            )
          })
        }
      </MainLayout.ContentContainer>
    </MainLayout.Container>
  )
}

const UserCard = styled.div`
  margin-bottom: 10px;
  padding: 15px;
  border-radius: 6px;
  background: ${({ theme }) => theme.layouts.secondary};
`;

const HeaderContainer = styled.div`
  display: flex;
  align-items: center;
`;

const UserCardTextContainer = styled.div`
  display: flex;
  flex: 1;
  margin-left: 12px;
  font-size: 18px;
`;

const LastMessageContainer = styled.div`
  display: flex;
  align-items: center;
  margin-left: 38px;
  margin-top: 2px;
  padding: 4px 8px;
  border-radius: 6px;
  background: ${({ theme }) => theme.layouts.hover};
  height: 34px;
`;

const LastMessageText = styled.div`
  color: ${({ theme }) => theme.typography.white};
  margin-left: 8px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`;

export default DialogsContainer

import React from 'react'
import Image from 'next/image';
import styled from 'styled-components'
import { useSelector, useDispatch } from 'react-redux'

import { getIsSameUsers, getProfile, getUser } from 'src/redux/profile/selectors'
import { setUser, setProfile } from 'src/redux/profile/actions';

import { api } from 'src/helpers/api';
import { useSocket } from 'src/hooks/useSocket';
import { useLoadUserData } from 'src/hooks/useLoadUserData';

import { TypeInput } from '@/components/input';
import { TypeToolButton } from '@/components/toolButton';
import { TypeIcon } from '@/components/icon';
import { MainLayout } from '@/components/layouts';
import PostContainer from './postContainer';
import { UpdateProfileName } from './updateName';
import UpdateProfileIcon from './updateIcon/updateProfileIcon';

import { User } from '@/types/user';
import { Profile } from '@/types/profile';
import { ModalState, TypeModalState } from '@/types/modal';
import sendPic from '../../../public/chevron_right_black_24dp.svg';

const index = () => {
  const [profileIconState, setProfileIconState] = React.useState<any>({
    isEdit: false,
    newIcon: null
  });
  const [modalState, setModalState] = React.useState<ModalState>(TypeModalState);
  const [textareaValue, setTextareaValue] = React.useState("");
  const [currentFileInput, setCurrentFileInput] = React.useState(null);
  const [users, usersQueries, fetchUser, addUser] = useLoadUserData();
  const profile: Profile = useSelector(getProfile);
  const user: User = useSelector(getUser);
  const isSameUsers: boolean = useSelector(getIsSameUsers);
  const profileUser = users[profile._id] || {};
  const dispatch = useDispatch();
  useSocket(user._id, "profile");

  React.useEffect(() => {
    if (!usersQueries.has(profile._id)) {
      fetchUser(profile._id);
    }
  }, [profile]);

  React.useEffect(() => {
    document.addEventListener('keydown', checkKeyDown);
    return function () {
      document.removeEventListener('keydown', checkKeyDown);
    }
  }, [textareaValue]);

  const checkKeyDown = React.useCallback((event) => {
    if (event.code === 'Enter') {
      handleSendComment();
    }
  }, [textareaValue, user, profile]);

  const handleChangeTextareaValue = React.useCallback((v) => {
    setTextareaValue(v);
  }, []);

  const handleSendComment = React.useCallback(() => {
    if (textareaValue !== "") {
      setTextareaValue("");
      api.post('profile/comment', { user, profileId: profile._id, comment: textareaValue }).then((res) => {
        if (res.success) {
          dispatch(setProfile(res.data));
        }
      })
    }
  }, [textareaValue, user, profile]);

  const handleOpenInputFile = React.useCallback((isUserIcon) => {
    if (isSameUsers) {
      setCurrentFileInput(isUserIcon);
      const input = document.getElementById('fileInput');
      input.click();
    }
  }, [isSameUsers]);

  const handleUploadFile = React.useCallback((event) => {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      const fileReader = new FileReader();
      if (!file) return;
      if (file.size > 1200000) return;

      fileReader.onload = (res) => {
        const img = document.createElement('img');
        //@ts-ignore
        img.src = res.target.result;
        img.style.width = "100%";

        img.onload = function () {
          const el = document.createElement('div');
          el.style.width = "100vw";
          el.style.maxWidth = "1200px";
          el.style.display = "flex";
          el.style.opacity = "0";
          el.append(img);
          document.body.appendChild(el);
          const height = el.clientHeight;
          document.body.removeChild(el);

          const iconSrc = res.target.result as string;
          if (currentFileInput) {
            api.post('set-icon/user', { userId: profile._id, iconSrc }).then(res => {
              if (res.success) {
                dispatch(setUser({ ...user, iconSrc }));
                addUser({ ...user, iconSrc });
              };
            });
          } else {
            const onCancel = () => {
              setProfileIconState({
                isEdit: false,
                newIcon: null
              });
            };
            const onSubmit = (iconPos: number) => {
              api.post('set-icon/profile', { userId: profile._id, iconSrc, iconPos }).then(res => {
                if (res.success) {
                  dispatch(setProfile({ ...profile, iconSrc, iconPos }));
                };
                setProfileIconState({
                  isEdit: false,
                  newIcon: null
                });
              });
            };
            setProfileIconState({
              isEdit: true,
              newIcon: iconSrc,
              iconHeight: height,
              onSubmit,
              onCancel
            });
          }
          const input = document.getElementById('fileInput') as HTMLInputElement;
          input.value = "";
        };
      }
      fileReader.readAsDataURL(file);
    }
  }, [user, profile, currentFileInput]);

  const handleCloseEditUsernameModal = React.useCallback(() => {
    setModalState(TypeModalState);
  }, []);
  const handleOpenEditUsernameModal = React.useCallback(() => {
    const onSubmit = (name) => {
      if (name.trim().length > 0) {
        api.post("set-name/user", { userId: user._id, name }).then(res => {
          if (res.success) {
            dispatch(setUser(res.data));
            addUser(res.data);
            setModalState(TypeModalState);
          }
        })
      }
    };
    setModalState({
      isOpen: true,
      label: "Изменить имя пользователя",
      onClose: handleCloseEditUsernameModal,
      onSubmit
    })
  }, []);

  return (
    <MainLayout.Container>
      <UpdateProfileIcon
        isEdit={profileIconState.isEdit}
        onIconClick={handleOpenInputFile.bind(null, false)}
        onSubmit={profileIconState.onSubmit}
        onCancel={profileIconState.onCancel}
        iconPos={profile.iconPos}
        icon={profile.iconSrc}
        newIconHeight={profileIconState.iconHeight}
        newIcon={profileIconState.newIcon}
      />
      <UpdateProfileName modalState={modalState} />
      <ProfileInfoContainer>
        <ProfileIconContainer>
          <TypeIcon
            onClick={handleOpenInputFile.bind(null, true)}
            isLoaded={!!profileUser.iconSrc}
            src={profileUser.iconSrc}
            width={170}
            height={170}
          />
        </ProfileIconContainer>
        <ProfileNameContainer>
          {profileUser && isSameUsers ? (
            <EditUsernameContainer onClick={handleOpenEditUsernameModal}>
              {user.name}
            </EditUsernameContainer>
          ) : profileUser.name}
        </ProfileNameContainer>
      </ProfileInfoContainer>
      <MainLayout.ContentContainer>
        <input onChange={handleUploadFile} type="file" id="fileInput" style={{ display: "none" }} />
        <Block>
          <BlockHeader>Публикации:</BlockHeader>
          <InputContainer>
            <TypeInput ph={isSameUsers ? `Что у вас нового, ${user.name}?` : `Напишите что-нибудь для ${profileUser.name || ""}`} value={textareaValue} onChange={handleChangeTextareaValue} />
            <ToolButtonWrapper>
              <TypeToolButton onClick={handleSendComment}>
                <Image src={sendPic} />
              </TypeToolButton>
            </ToolButtonWrapper>
          </InputContainer>
          <PostContainer />
        </Block>
      </MainLayout.ContentContainer>
    </MainLayout.Container>
  )
}

const ProfileInfoContainer = styled.div`
  display: flex;
  height: 70px;
  flex-direction: column;
  align-items: flex-end;
  justify-content: flex-end;
  position: relative;

  @media (min-width: 768px) {
    height: 80px;
  };

  @media (min-width: 1200px) {
  height: 90px;
  };
`;
const ProfileIconContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: calc(174px * 0.75);
  height: calc(174px * 0.75);
  position: absolute;
  top: calc(-130px * 0.75);
  left: calc(50% - 91px * 0.75);
  background: ${({ theme }) => theme.layouts.primary};
  border: solid ${({ theme }) => theme.layouts.primary} 4px;
  border-radius: 50%;

  @media (min-width: 768px) {
    width: calc(174px * 0.85);
    height: calc(174px * 0.85);
    top: calc(-130px * 0.85);
    left: calc(50% - 91px * 0.85);
  };

  @media (min-width: 1200px) {
    width: 174px;
    height: 174px;
    top: -130px;
    left: calc(50% - 91px);
  };
`;
const ProfileNameContainer = styled.div`
  display: flex;
  margin: 0 auto;
  font-size: 18px;
  letter-spacing: 1px;
  color: ${({ theme }) => theme.typography.white};

  @media (min-width: 768px) {
    font-size: 22px;
  };

  @media (min-width: 1200px) {
  font-size: 28px;
  };
`;

const Block = styled.div`
  margin: 10px 0;
`;

const BlockHeader = styled.div`
  font-size: 14px;
  color: ${({ theme }) => theme.typography.white}};
  letter-spacing: 1px;
  margin-bottom: 10px;

  @media (min-width: 768px) {
    font-size: 16px;
  };

  @media (min-width: 1200px) {
    font-size: 20px;
  };
`;

const EditUsernameContainer = styled.div`
  letter-spacing: 1px;
  color: ${({ theme }) => theme.typography.white};
  border-bottom: 4px dotted transparent;
  position: relative;
  top: 2px;
  cursor: pointer;

  &:hover {
    top: 0px;
    color: ${({ theme }) => theme.colors.primary};
    border-bottom: 4px dotted ${({ theme }) => theme.colors.primary};
  }
`;

const InputContainer = styled.div`
  display: flex;
  width: 100%;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 20px;
`;

const ToolButtonWrapper = styled.div`
  display: flex;
  margin-left: 10px;
`;

export default index;

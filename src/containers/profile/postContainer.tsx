import Link from 'next/link';
import React from 'react'
import styled from 'styled-components';
import dayjs from 'dayjs';
import { useSelector, useDispatch } from 'react-redux';

import { api } from '@/helpers/api';
import { useLoadUserData } from 'src/hooks/useLoadUserData';

import { TypeIcon } from '@/components/icon';
import { TypeButton } from '@/components/button';
import { TypeGroupButton } from '@/components/button/groupButton';
import { TypeLinkText } from '@/components/text';

import { getProfile, getUser } from '@/redux/profile/selectors';
import { setProfile } from '@/redux/profile/actions';

import { User } from '@/types/user';
import { Comment, Profile } from '@/types/profile'

const postContainer = () => {
  const [users, usersQueries, fetchUser] = useLoadUserData();
  const profile: Profile = useSelector(getProfile);
  const user: User = useSelector(getUser);
  const dispatch = useDispatch();

  const handleDeleteComment = React.useCallback((commentId) => {
    api.post('profile/delete-comment', { commentId, profileId: profile._id }).then((res) => {
      if (res.success) {
        dispatch(setProfile(res.data));
      }
    })
  }, [profile.comments]);

  const handleLikeComment = React.useCallback((commentId) => {
    api.post('profile/like-comment', { commentId, userId: user._id, profileId: profile._id }).then((res) => {
      if (res.success) {
        dispatch(setProfile(res.data));
      }
    })
  }, [profile.comments]);

  return <>
    {
      [...profile.comments].reverse().map((comment: Comment) => {
        const userLikedThisPost = !!comment.likes.find(like => like.authorId === user._id);
        const senderUser = users[comment.authorId];
        const iconSrc = users[comment.authorId]?.iconSrc;
        if (!usersQueries.has(comment.authorId)) {
          fetchUser(comment.authorId);
        };

        return (
          <CommentCard key={comment._id}>
            <HeaderContainer>
              <HeaderAuthor>
                <Link href={`/profile/${comment.authorId}`}>
                  <a>
                    <TypeIcon src={iconSrc} isLoaded={!!iconSrc} />
                  </a>
                </Link>
                <Link href={`/profile/${comment.authorId}`}>
                  <a>
                    {senderUser && <HeaderTextContainer><TypeLinkText>{senderUser.name}</TypeLinkText></HeaderTextContainer>}
                  </a>
                </Link>
              </HeaderAuthor>
              <HeaderDate>
                {dayjs(comment.date).format('DD.MM.YYYY HH:MM:ss')}
              </HeaderDate>
            </HeaderContainer>
            <CommentCardContent>{comment.comment}</CommentCardContent>
            <FooterContainer>
              <TypeGroupButton
                active={false}
                onClick={handleLikeComment.bind(null, comment._id)}
                firstButtonContent={userLikedThisPost ? 'Не нравится' : 'Нравится'}
                secondButtonContent={comment.likesCount}
              />
              {
                (user._id === comment.authorId || user._id === profile._id) && (
                  <TypeButton
                    onClick={handleDeleteComment.bind(null, comment._id)}
                  >
                    Удалить
                  </TypeButton>
                )
              }
            </FooterContainer>
          </CommentCard>)
      }
      )
    }
  </>
}

const CommentCard = styled.div`
  border-radius: 6px;
  padding: 15px;
  background: ${({ theme }) => theme.layouts.secondary};
  margin-bottom: 10px;
`;
const HeaderContainer = styled.div`
  display: flex;
  justify-content: space-between;
  padding-bottom: 6px;
`;
const HeaderAuthor = styled.div`
  display: flex;
  align-items: center;
`;
const HeaderDate = styled.div`
  color: ${({ theme }) => theme.typography.hint};
  font-size: 10px;
  display: flex;
  align-items: center;
  letter-spacing: 1px;

  @media (min-width: 768px) {
    font-size: 12px;
  };

  @media (min-width: 1200px) {
    font-size: 14px;
  };
`;
const HeaderTextContainer = styled.div`
  margin-left: 8px;
  font-size: 12px;

  @media (min-width: 768px) {
    font-size: 14px;
  };

  @media (min-width: 1200px) {
    font-size: 16px;
  };
`;
const CommentCardContent = styled.div`
  font-size: 12px;
  color: ${({ theme }) => theme.typography.white};
  letter-spacing: 1px;
  padding-bottom: 12px;

  @media (min-width: 768px) {
    font-size: 14px;
  };

  @media (min-width: 1200px) {
    font-size: 16px;
  };
`;
const FooterContainer = styled.div`
  display: flex;
  justify-content: space-between;
`;

export default postContainer

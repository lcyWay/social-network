import React from 'react'
import styled from 'styled-components';

interface iProps {
  newIcon: string;
  height: number;
  submit?: boolean;
  onSubmit?: (v: number) => void;
};

export const ProfileDraggableIcon: React.FC<iProps> = ({ newIcon, submit, onSubmit, height }) => {
  const [top, setTop] = React.useState(0);
  const maxTop = React.useMemo(() => height - (window.innerWidth >= 1400 ? 300 : window.innerWidth >= 768 ? 200 : 100), [height]);

  React.useEffect(() => {
    setTop(top => {
      submit && onSubmit(top / maxTop * 100);
      return 0;
    })
  }, [submit]);

  React.useEffect(() => {
    setTop(0);
    const element = document.getElementById("profile-icon-preview");
    let prev = null;

    element.onpointerdown = () => {
      element.ondragstart = () => false
      document.addEventListener('pointermove', handleDrag);

      document.onpointerup = function () {
        document.removeEventListener('pointermove', handleDrag);
        element.onpointerup = null;
        prev = null;
      };
    };

    function handleDrag(event) {
      if (prev === event.clientY) return;
      if (prev === null) {
        prev = event.clientY;
        return;
      }

      if (prev > event.clientY) {
        const dif = prev - event.clientY;
        setTop(top => {
          const v = top + dif >= maxTop ? maxTop : top + dif;
          element.style.top = -v + "px";
          return v;
        });
      } else {
        const dif = event.clientY - prev;
        setTop(top => {
          const v = top - dif <= 0 ? 0 : top - dif;
          element.style.top = -v + "px";
          return v;
        });
      }
      prev = event.clientY;
    };
  }, []);

  return (
    <IconPreviewContainer height={height} id="profile-icon-preview" alt="icon" src={newIcon} />
  )
}

const IconPreviewContainer = styled.img<{ height: number }>`
  position: absolute;
  top: 0px;
  left: 0;
  background-size: cover;
  display: flex;
  width: 100%;
  height: ${({ height }) => height + "px"};
  object-fit: fill;
  transition: unset;
  cursor: move;
`;

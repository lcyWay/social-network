import React from 'react'
import Image from 'next/image'
import styled from 'styled-components';
import { useSelector } from 'react-redux';

import { getIsSameUsers } from '@/redux/profile/selectors';

import { TypeButton } from '@/components/button';
import { ProfileDraggableIcon } from './profileDraggableIcon';

import dragIcon from '../../../../public/open_with_white_18dp.svg'

interface iProps {
  icon: string;
  iconPos: number;
  newIcon: string;
  newIconHeight: number;
  onSubmit?: (data: any) => void;
  onCancel?: () => void;
  onIconClick: () => void;
  isEdit: boolean;
};

const UpdateProfileIcon: React.FC<iProps> = ({ icon, isEdit, newIcon, iconPos, onIconClick, onSubmit, onCancel, newIconHeight }) => {
  const [submit, setSubmit] = React.useState(false);
  const isSameUsers = useSelector(getIsSameUsers);

  const handleSubmitWithData = React.useCallback((v: number) => {
    onSubmit(v);
    setSubmit(false);
  }, [onSubmit]);
  const handlePreSubmit = React.useCallback(() => {
    setSubmit(true);
  }, []);

  const handleCancel = React.useCallback(() => {
    onCancel();
  }, [onCancel]);

  return (
    <Container>
      {
        isEdit
          ? <>
            <PreviewLabel>
              <Image src={dragIcon} />
              <LabelText>
                Перетащите, чтобы изменить положение
              </LabelText>
              <ButtonsContainer>
                <TypeButton margin="0 20px" onClick={handleCancel}>Отмена</TypeButton>
                <TypeButton onClick={handlePreSubmit}>Сохранить</TypeButton>
              </ButtonsContainer>
            </PreviewLabel>
            <ProfileDraggableIcon submit={submit} height={newIconHeight} onSubmit={handleSubmitWithData} newIcon={newIcon} />
          </>
          : <ProfileLayoutContainer
            isSameUsers={isSameUsers}
            onClick={onIconClick}
            iconPos={iconPos}
            icon={icon}
          />
      }
    </Container>
  )
}

const Container = styled.div`
  position: relative;
  max-width: 1200px;
  width: 100%;
  margin: auto;
  border-radius: 6px;
  height: 100px;
  overflow: hidden;

  @media (min-width: 768px) {
    height: 200px;
  };

  @media (min-width: 1200px) {
    height: 300px;
  };
`;

const PreviewLabel = styled.div`
  position: absolute;
  background: rgba(0,0,0,0.6);
  width: calc(100% - 20px);
  height: 48px;
  padding: 0 10px;
  display: flex;
  align-items: center;
  color: ${({ theme }) => theme.typography.white};
  top: 0;
  left: 0;
  z-index: 20;
`;
const LabelText = styled.div`
  flex: 1;
  margin-left: 5px;
  font-size: 10px;

  @media (min-width: 768px) {
    font-size: 12px;
  };

  @media (min-width: 1200px) {
    font-size: 16px;
  };
`;
const ButtonsContainer = styled.div`
  display: flex;
`;

const ProfileLayoutContainer = styled.div<{ icon: string, iconPos: number, isSameUsers: boolean }>`
  background-image: ${({ icon }) => icon ? `url(${icon})` : "linear-gradient(45deg, #4e4e4e, #2b2b2df7)"};
  background-position: 0 ${({ iconPos }) => iconPos + "%"};
  width: 100%;
  height: 100%;
  background-size: cover;
  cursor: ${({ isSameUsers }) => isSameUsers ? "pointer" : "auto"};
`;

export default UpdateProfileIcon

import React from 'react'
import styled from 'styled-components';

import { TypeInput } from '@/components/input';
import { TypeModal } from '@/components/modal';
import { ModalState } from '@/types/modal';

interface iProps {
  modalState: ModalState;
};

export const UpdateProfileName: React.FC<iProps> = ({ modalState }) => {
  const [name, setName] = React.useState("");

  React.useEffect(() => {
    const handleKeyDown = (event) => {
      if (event.code === "Enter") {
        handleSubmit();
      }
    };
    if (modalState.isOpen) {
      const input = document.getElementById('profile-name-editor');
      input.focus();
      document.addEventListener('keydown', handleKeyDown);
    }
    return function () {
      document.removeEventListener('keydown', handleKeyDown);
    }
  }, [modalState, name]);

  const handleCancel = React.useCallback(() => {
    setName("");
    modalState.onClose();
  }, [modalState]);
  const handleSubmit = React.useCallback(() => {
    modalState.onSubmit(name);
    setName("")
  }, [modalState, name]);

  return (
    <TypeModal
      isOpen={modalState.isOpen}
      label={modalState.label}
      onClose={handleCancel}
      onSubmit={handleSubmit}
    >
      <Container>
        <Title>Изменение увидят все пользователи:</Title>
        <TypeInput
          id="profile-name-editor"
          value={name}
          ph="Начните вводить текст..."
          onChange={setName}
          staticColor="secondary"
          focusedColor="primary"
        />
      </Container>
    </TypeModal>
  )
}

const Container = styled.div`
  padding: 40px 20px;
  width: calc(100% - 40px);
  background: ${({ theme }) => theme.layouts.secondary};
`;

const Title = styled.div`
  font-size: 14px;
  margin-bottom: 5px;
  color: ${({ theme }) => theme.typography.hint};
`;

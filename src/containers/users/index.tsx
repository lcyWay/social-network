import React from 'react'
import Image from 'next/image'
import styled from 'styled-components';

import { useSocket } from '@/hooks/useSocket';

import { TypeOnlineStatus } from '@/components/online';
import { MainLayout } from '@/components/layouts';
import { TypeInput } from '@/components/input';
import { TypeIcon } from '@/components/icon';
import { TypeLink } from '@/components/link';
import { TypeLinkText } from '@/components/text';
import { TypeToolButton } from '@/components/toolButton';
import { TypeHeader } from '@/components/header/typeHeader';

import sendPic from '../../../public/chevron_right_black_24dp.svg';
import { User } from '@/types/user';

const index = ({ userArrayData, user }) => {
  const [filterValue, setFilterValue] = React.useState("");
  useSocket(user._id, "users");

  return (
    <MainLayout.Container>
      <TypeHeader>
        <TypeInput
          staticColor="secondary"
          value={filterValue}
          onChange={setFilterValue}
          ph="Поиск по имени..."
        />
      </TypeHeader>
      <MainLayout.ContentContainer>
        <UsersContainer>
          {userArrayData.map((userData: User) => {
            const isSelfUserCard = userData._id === user._id;
            return (
              <UserCard key={userData._id}>
                <TypeLink href={`/profile/${userData._id}`}>
                  <TypeIcon
                    src={userData.iconSrc}
                    isLoaded={!!userData.iconSrc}
                    width={30}
                  />
                </TypeLink>
                <UserCardTextContainer>
                  <TypeLink href={`/profile/${userData._id}`}>
                    <TypeLinkText>{userData.name}</TypeLinkText>
                  </TypeLink>
                  <TypeOnlineStatus status={isSelfUserCard || userData.online} />
                </UserCardTextContainer>
                <TypeLink href={`/profile/${userData._id}`}>
                  <TypeToolButton hoverColor="primary">
                    <Image src={sendPic} />
                  </TypeToolButton>
                </TypeLink>
              </UserCard>
            )
          })}
        </UsersContainer>
      </MainLayout.ContentContainer>
    </MainLayout.Container>
  )
}

const UsersContainer = styled.div`
  width: calc(100% - 10px);
  max-width: 800px;
  margin: auto;
  padding: 10px 5px;
`;

const UserCard = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 10px;
  padding: 15px;
  border-radius: 6px;
  display: flex;
  background: ${({ theme }) => theme.layouts.secondary};
`;
const UserCardTextContainer = styled.div`
  display: flex;
  flex: 1;
  margin-left: 12px;
  font-size: 18px;
`;

export default index

import { serverURL } from "src/constants/server";

class Api {
  constructor() {
    this.mainPath = serverURL;
  }

  async get(url) {
    return await fetch(this.mainPath + url).then(res => res.json()).then(data => data);
  }

  async post(url, data) {
    return await fetch(this.mainPath + url, {
      method: 'post',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data)
    }).then(res => res.json()).then(data => data);
  }
}

const api = new Api();
export { api };

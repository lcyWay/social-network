import { api } from 'src/helpers/api';
import { ResponceData } from '@/types/api'
import { User } from '@/types/user';
import cookies from 'next-cookies'
import { GetServerSidePropsContext } from 'next';

export const checkAuth = async (ctx: GetServerSidePropsContext, isDialogsPage?: boolean) => {
  const { authCpwToken } = cookies(ctx);
  if (authCpwToken) {
    const res: ResponceData<User> = await api.post('auth', { token: authCpwToken, isDialogsPage });
    return res.success ? res.data : null;
  } else {
    return null
  }
};

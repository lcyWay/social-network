import React from "react";
import { api } from "@/helpers/api";
import { User } from "@/types/user";

type iReturn = [
  users: any,
  usersQueries: any,
  fetchUser: (id: string) => void,
  addUser: (user: User) => void
];

export const useLoadUserData = (): iReturn => {
  const [users, setUsersData] = React.useState({});
  const usersQueries = React.useMemo(() => new Set(), []);

  const fetchUser = React.useCallback((id: string) => {
    if (!usersQueries.has(id)) {
      usersQueries.add(id);
      api.post('user', { id }).then(res => {
        if (res.success) {
          setUsersData(prev => ({ ...prev, [id]: res.data }))
        }
      })
    };
  }, [usersQueries]);

  const addUser = React.useCallback((user: User) => {
    usersQueries.add(user._id);
    setUsersData(prev => ({ ...prev, [user._id]: user }));
  }, [usersQueries]);

  return [
    users,
    usersQueries,
    fetchUser,
    addUser
  ];
};
import { useNotification } from "@/components/notification";
import { SocketNotification } from "@/types/socket";
import React from "react";
import { io, Socket } from "socket.io-client";
import { serverURL } from 'src/constants/server';

type iReturn = [
  socket: Socket,
];

export const useSocket = (
  id: string,
  destination: "dialogs" | "dialog" | "profile" | "users",
  locationId?: string
): iReturn => {
  const socket = React.useMemo(() => io(serverURL), []);
  const { showNotification } = useNotification();

  React.useEffect(() => {
    socket.emit('SET_USER_ONLINE', id);
    socket.on('USER_NOTIFICATION', (message: SocketNotification) => {
      if (
        (locationId && destination === message.destination && locationId !== message.locationId) ||
        (destination !== message.destination)
      ) {
        showNotification(message);
      }
      if (locationId === message.locationId && message.destination === destination && destination === "dialog") {
        socket.emit('VIEW_MESSAGES');
      }
    });

    return function () {
      socket.disconnect();
    }
  }, []);

  return [
    socket
  ];
};

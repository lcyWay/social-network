import React from 'react';
import { Provider } from 'react-redux';
import { ThemeProvider } from 'styled-components';
import type { AppProps } from 'next/app';

import { theme } from '../theme'
import { store } from '../redux/store';

import { NotificationProvider } from '@/components/notification';

import '../styles/index.css';

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <Provider store={store}>
      <ThemeProvider theme={theme}>
        <NotificationProvider>
          <Component {...pageProps} />
        </NotificationProvider>
      </ThemeProvider>
    </Provider>
  )
}

export default MyApp

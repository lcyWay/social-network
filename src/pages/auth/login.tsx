import React from 'react'
import router from 'next/router';
import jwt from 'jsonwebtoken'
import { api } from '../../helpers/api'
import { GetServerSideProps } from 'next';
import { checkAuth } from '../../helpers/auth'

const Login: React.FC = () => {
  const [email, setEmail] = React.useState("")
  const [password, setPassword] = React.useState("")

  const handleLogin = () => {
    api.post('auth/login', { email, password }).then(res => {
      if (res.success) {
        const token = jwt.sign({ email, password }, process.env.NEXT_PUBLIC_JWT_SECRET_KEY);
        document.cookie = `authCpwToken=${token}; path=/`;
        router.replace(`/profile/${res.data._id}`);
      }
    });
  };

  return (
    <div>
      <p>Авторизация</p>
      <input value={email} onChange={e => setEmail(e.target.value)} />
      <input value={password} onChange={e => setPassword(e.target.value)} />
      <button onClick={handleLogin}>Войти</button>
    </div>
  )
}

//@ts-ignore
export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const user = await checkAuth(ctx);

  return user !== null ? {
    redirect: {
      destination: `/profile/${user._id}`
    }
  } : {
    props: {}
  }
};

export default Login

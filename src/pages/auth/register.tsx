import React from 'react'
import { GetServerSideProps } from 'next';
import { checkAuth } from '../../helpers/auth'

const register = () => {
  return (
    <div>
      регистрация
    </div>
  )
}

//@ts-ignore
export const getServerSideProps: GetServerSideProps = async (context) => {
  const isLoggined = await checkAuth(context);

  return isLoggined !== null ? {
    redirect: {
      destination: '/'
    }
  } : {
    props: {}
  }
};

export default register

import React from 'react'
import { GetServerSideProps } from 'next';

import DialogContainer from '@/containers/dialog';
import { checkAuth } from 'src/helpers/auth';
import { User } from '@/types/user';

import Header from '@/components/header';
import Navbar from '@/components/navbar';
import NewsBar from '@/components/newsbar';

const DialogPage: React.FC<{ user: User, dialogId: string }> = ({ user, dialogId }) => {
  return <div>
    <Header user={user} />
    <div className="flex">
      <Navbar />
      <DialogContainer user={user} dialogId={dialogId} />
      <NewsBar />
    </div>
  </div>
}

//@ts-ignore
export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const user = await checkAuth(ctx, true);
  if (user === null) {
    return {
      redirect: {
        destination: '/auth/login'
      }
    }
  }

  return {
    props: {
      dialogId: ctx.query.id,
      user
    }
  }
};

export default DialogPage

import React from 'react'
import { GetServerSideProps } from 'next';

import DialogsContainer from '@/containers/dialogs';
import { checkAuth } from '@/helpers/auth';
import { User } from '@/types/user';

import Header from '@/components/header';
import Navbar from '@/components/navbar';
import NewsBar from '@/components/newsbar';

const Dialogs: React.FC<{ user: User }> = ({ user }) => {
  return <div>
    <Header user={user} />
    <div className="flex">
      <Navbar />
      <DialogsContainer user={user} />
      <NewsBar />
    </div>
  </div>
}

//@ts-ignore
export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const user = await checkAuth(ctx, true);
  if (user === null) {
    return {
      redirect: {
        destination: '/auth/login'
      }
    }
  }

  return {
    props: {
      user
    }
  }
};

export default Dialogs

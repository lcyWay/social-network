import React from 'react';
import { GetServerSideProps } from 'next';
import { checkAuth } from 'src/helpers/auth';

const Home: React.FC = () => {
  return (
    <div>Главная</div>
  )
}

//@ts-ignore
export const getServerSideProps: GetServerSideProps = async (context) => {
  const user = await checkAuth(context);
  return user ? {
    redirect: {
      destination: `/profile/${user._id}`
    }
  } : {
    redirect: {
      destination: '/auth/login'
    }
  }
};

export default Home;

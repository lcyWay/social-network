import React from 'react'
import { api } from 'src/helpers/api';
import { checkAuth } from 'src/helpers/auth';
import { GetServerSideProps } from 'next';
import { User } from '@/types/user';
import Header from '../../components/header'
import Navbar from '../../components/navbar'
import NewsBar from '@/components/newsbar';
import ProfileContainer from '../../containers/profile'
import { useSelector, useDispatch } from 'react-redux'
import { isLoading as profileIsLoading } from 'src/redux/profile/selectors';
import { initPage } from 'src/redux/profile/actions';
import { Profile } from '@/types/profile';

const ProfilePage: React.FC<{ user: User, profile: Profile }> = ({ profile, user }) => {
  const ProfileStateIsLoading = useSelector(profileIsLoading);
  const dispatch = useDispatch();

  React.useEffect(() => {
    dispatch(initPage({ user, profile }));
  }, [profile._id, user._id]);

  return (
    <div>
      <Header user={user} />
      <div className="flex">
        <Navbar />
        {
          ProfileStateIsLoading ? "Загрузка" : <ProfileContainer />
        }
        <NewsBar />
      </div>
    </div>
  )
}

//@ts-ignore
export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const user = await checkAuth(ctx);
  if (user === null) {
    return {
      redirect: {
        destination: '/auth/login'
      }
    }
  }

  const res = await api.post('profile', { id: ctx.params.id });
  if (!res.success) {
    return {
      redirect: {
        destination: `/profile/${user._id}`
      }
    }
  }

  return {
    props: {
      profile: res.data,
      user
    }
  }
};

export default ProfilePage;

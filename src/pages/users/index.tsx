import React from 'react'

import { api } from 'src/helpers/api';
import { checkAuth } from 'src/helpers/auth';
import Header from '@/components/header';
import Navbar from '@/components/navbar';
import NewsBar from '@/components/newsbar';
import UsersContainer from '@/containers/users'

import { User } from '@/types/user';
import { GetServerSideProps } from 'next'

type iProps = {
  users: User[],
  selfUser: User
};

const Users: React.FC<iProps> = ({ users, selfUser }) => {
  return (
    <div>
      <Header user={selfUser} />
      <div className="flex">
        <Navbar />
        <UsersContainer user={selfUser} userArrayData={users} />
        <NewsBar />
      </div>
    </div>
  )
}

//@ts-ignore
export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const user = await checkAuth(ctx);

  if (user === null) {
    return {
      redirect: {
        destination: '/auth/login'
      }
    }
  }

  const res = await api.get('users');
  if (!res.success) {
    return {
      redirect: {
        destination: `/profile/${user._id}`
      }
    }
  }

  return {
    props: {
      users: res.data,
      selfUser: user
    }
  }
};

export default Users

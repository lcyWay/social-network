import { Profile } from "@/types/profile";
import { User } from "@/types/user";

export const actions = {
  INIT_PAGE: "INIT_PAGE",
  SET_PROFILE: "SET_PROFILE",
  SET_USER: "SET_PROFILE_USER"
};

export const initPage = ({ user, profile }: { user: User, profile: Profile }) => ({
  type: actions.INIT_PAGE,
  data: { user, profile }
})

export const setUser = (user: User) => ({
  type: actions.SET_USER,
  data: user
})

export const setProfile = (profile: Profile) => ({
  type: actions.SET_PROFILE,
  data: profile
})

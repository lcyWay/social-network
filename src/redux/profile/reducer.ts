import { actions } from "./actions";
import { SelfState } from "./types";


const initialState: SelfState = {
  isLoading: true,
  isSameUsers: false,
  user: null,
  profile: null
};

function profileReducer(state = initialState, action) {
  switch (action.type) {
    case actions.INIT_PAGE: {
      const { user, profile } = action.data;
      return {
        ...state,
        isLoading: false,
        isSameUsers: user._id === profile._id,
        user,
        profile
      }
    };
    case actions.SET_USER: {
      return {
        ...state,
        user: action.data
      }
    };
    case actions.SET_PROFILE: {
      return {
        ...state,
        profile: action.data
      }
    };

    default:
      return state
  }
}

export { profileReducer };

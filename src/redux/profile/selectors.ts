import { createSelector } from 'reselect'
import { SelfState } from './types';

export const selfState = (state) => state.Profile;

export const getProfile = createSelector(selfState, (state: SelfState) => state.profile);
export const getUser = createSelector(selfState, (state: SelfState) => state.user);
export const getIsSameUsers = createSelector(selfState, (state: SelfState) => state.isSameUsers);
export const isLoading = createSelector(selfState, (state: SelfState) => state.isLoading);

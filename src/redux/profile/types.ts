import { Profile } from "@/types/profile";
import { User } from "@/types/user";

export type SelfState = {
  isLoading: boolean,
  isSameUsers: boolean,
  user: null | User,
  profile: null | Profile
}

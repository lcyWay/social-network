export const theme = {
  layouts: {
    primary: '#363639',
    secondary: '#323234',
    hover: '#4e4e4e',
    white: '#eee'
  },
  typography: {
    white: '#fff',
    hint: '#717171',
    lightDark: '#2d2d2d',
  },
  colors: {
    primary: '#5572ff',
    secondary: '#4e577f',
    green: '#44cb44',
    gray: '#7b7b7b',
  }
};

export type ResponceData<T> = {
  success: boolean;
  data: T;
};
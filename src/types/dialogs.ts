export type Dialog = {
  _id: string;
  companionId: string;
  unreadMessages: number;
  lastMessage: Message;
};

export type DialogMessages = {
  _id: string;
  messages: Message[];
};

export type Message = {
  _id: string;
  userId: string;
  message: string;
  date: Date;
};

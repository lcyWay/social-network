export interface ModalState {
  isOpen: boolean;
  label: string | null;
  data?: any;
  onSubmit?: (data?: any) => void;
  onClose?: () => void;
};

export const TypeModalState: ModalState = {
  isOpen: false,
  label: null,
};
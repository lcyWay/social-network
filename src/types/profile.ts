export type Profile = {
  _id: string,
  iconSrc: string | null,
  iconPos: number,
  comments: Comment[]
};

export type Comment = {
  authorId: string,
  likes: Like[],
  likesCount: number,
  comment: string,
  date: string,
  _id: string
};

export type Like = {
  _id: string,
  authorId: string
};

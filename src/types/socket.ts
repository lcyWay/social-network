import { Message } from "./dialogs";

export type SocketNotification = {
  type: "message",
  destination: "dialog" | "dialogs" | "profile",
  locationId?: string,
  data: Message,
};
import { Dialog } from "./dialogs";

export type ShortUser = Omit<User, "dialogs">;

export type User = {
  _id: string;
  name: string;
  online: boolean;
  iconSrc: string | null;
  dialogs?: Dialog[];
};
